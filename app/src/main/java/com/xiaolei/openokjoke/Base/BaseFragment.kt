package com.xiaolei.openokjoke.Base

import com.xiaolei.easyfreamwork.base.BaseFragment
import com.xiaolei.openokjoke.BuildConfig

/**
 * Created by xiaolei on 2017/12/6.
 */
public abstract class BaseFragment : BaseFragment()
{
    override fun onSetContentView()
    {
        setStatusBar(true)
    }
    
    fun setStatusBar(lightStatusBar: Boolean)
    {
        
    }

    override abstract fun contentViewId(): Int
    override abstract fun initObj()
    override abstract fun initView()
    override abstract fun initData()
    override abstract fun setListener()
    override abstract fun loadData()
    /**
     * 只有在DEBUG模式下，才会弹出
     *
     * @param object
     */
    fun AlertDebug(`object`: Any)
    {
        if (BuildConfig.DEBUG)
        {
            Alert(`object`)
        }
    }
}