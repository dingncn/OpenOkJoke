package com.xiaolei.openokjoke.Activitys

import android.os.Bundle
import com.xiaolei.openokjoke.Base.BaseActivity
import com.xiaolei.openokjoke.Exts.enqueue
import com.xiaolei.openokjoke.Net.APPNet
import com.xiaolei.openokjoke.Net.BaseRetrofit
import com.xiaolei.openokjoke.R
import kotlinx.android.synthetic.main.activity_feedback.*

/**
 * 反馈界面
 * Created by xiaolei on 2018/3/12.
 */
class FeedBackActivity : BaseActivity()
{
    private val appNet by lazy {
        BaseRetrofit.create(APPNet::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        setContentView(R.layout.activity_feedback)
        super.onCreate(savedInstanceState)
    }

    override fun initObj()
    {

    }

    override fun initView()
    {

    }

    override fun initData()
    {

    }

    override fun setListener()
    {
        app_title.setOnLeftImageClick {
            finish()
        }
        commit_btn.setOnClickListener {
            val contextStr = context_et.text.toString().trim()
            val contactStr = contact_et.text.toString().trim()
            if (contextStr.isEmpty())
            {
                Toast("反馈内容不能为空")
                return@setOnClickListener
            }
            commit_btn.isClickable = false
            val call = appNet.feedBack(contextStr, contactStr)
            call.enqueue(this, { result ->
                Toast(result.message)
                if (result.isOk())
                {
                    finish()
                }
            }, {
                commit_btn.isClickable = true
            })
        }
    }

    override fun loadData()
    {

    }
}